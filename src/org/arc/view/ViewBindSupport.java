/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.arc.view;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author ettore
 */
public class ViewBindSupport {

    /**
     * @see android.app.Activity#findViewById(int)
     */
    public static final String ANDROID_VIEW_FIND_BY_ID = "findViewById";

    /**
     * @see android.app.Activity
     */
    private final Object activity;
    /**
     * @see android.R
     */
    private final Class<?> R;
    /**
     * @see android.R.id
     */
    private Class<?> idClass;

    /**
     * @see android.app.Activity#findViewById(int)
     */
    private Method findViewMethod;

    /**
     *
     * @param activity android.app.Activity instance
     * @param resource like a android.R
     */
    public ViewBindSupport(Object activity, Class<?> resource) {
        this.activity = activity;
        this.R = resource;
        this.setIdClass();
        this.setFindViewMethod();
        this.bindFields(this.makeBindMap());
    }

    private void setIdClass() {
        for (Class<?> clazz : R.getDeclaredClasses()) {
            if (!clazz.getSimpleName().equals("id")) {
                continue;
            }
            this.idClass = clazz;
            return;
        }
        throw new RuntimeException(String.format("The %s class not have the id class", R.getName()));
    }

    private void setFindViewMethod() {
        try {
            this.findViewMethod = this.activity.getClass().getMethod(ANDROID_VIEW_FIND_BY_ID, int.class);
        }
        catch (NoSuchMethodException | SecurityException ex) {
            throw new RuntimeException(ex);
        }
    }

    private int getIdByIdName(String idName) {
        try {
            Field field = this.idClass.getField(idName);
            return field.getInt(null);
        }
        catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
            throw new RuntimeException(ex);
        }
    }

    private Map<String, Field> makeBindMap() {
        Class<?> clazz = this.activity.getClass();
        Map<String, Field> bindMap = new HashMap<>();
        for (Field field : clazz.getDeclaredFields()) {
            Bind bind = field.getAnnotation(Bind.class);
            if (bind == null) {
                continue;
            }
            String bindName = bind.value();
            if (bindName.isEmpty()) {
                bindName = field.getName();
            }
            bindMap.put(bindName, field);
        }
        return bindMap;
    }

    private Object findViewById(int id) {
        try {
            return this.findViewMethod.invoke(this.activity, id);
        }
        catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            throw new RuntimeException(ex);
        }
    }

    private void bindFields(Map<String, Field> bindMap) {
        for (String bindName : bindMap.keySet()) {
            Field field = bindMap.get(bindName);
            int viewId = this.getIdByIdName(bindName);
            Class<?> fieldType = field.getType();
            Object view = findViewById(viewId);
            if (view == null) {
                throw new RuntimeException(String.format("View with id = %d not found", viewId));
            }
            try {
                boolean accessible = field.isAccessible();
                field.setAccessible(true);
                field.set(this.activity, fieldType.cast(view));
                field.setAccessible(accessible);
            }
            catch (IllegalArgumentException | IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        }
    }

}
