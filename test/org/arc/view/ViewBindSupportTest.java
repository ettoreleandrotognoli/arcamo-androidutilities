/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.arc.view;

import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author ettore
 */
public class ViewBindSupportTest {

    @Test
    public void testBind() {
        VirtualActivity activity = new VirtualActivity();
        Assert.assertNotNull(activity.getBtStart());
        Assert.assertNotNull(activity.getBtStop());
    }

    static class VirtualActivity {

        @Bind
        private ArrayList btStop;

        @Bind("startButton")
        private ArrayList btStart;

        public List findViewById(int id) {
            return new ArrayList();
        }

        public VirtualActivity() {
            ViewBindSupport viewBindSupport = new ViewBindSupport(VirtualActivity.this, R.class);
        }

        public List getBtStop() {
            return btStop;
        }

        public List getBtStart() {
            return btStart;
        }
    }

    static class R {

        static class id {

            public static final int btStop = 0x01;
            public static final int startButton = 0x02;
        }

    }

}
